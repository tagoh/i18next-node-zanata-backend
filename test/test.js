'use strict';
const assert = require('assert');
const backend = require('../lib/index.js');
const i18next = require('i18next');

describe('i18next', function() {
  describe('zanata', function() {
    it('Translate strings through Zanata', function(done) {
      this.timeout(5000);
      let p = i18next
        .createInstance()
        .use(backend)
        .init({
          backend: {
            url: 'https://translate.stage.zanata.org',
            project: 'i18next-node-zanata-backend',
            'project-version': 'master',
            'project-type': 'gettext',
            cachedir: './test'
          },
          nsSeparator: false,
          keySeparator: false,
          lng: 'ja',
          fallbackLng: 'en'
        });
      p
        .on('loaded', () => {
          assert.notStrictEqual(p.t('Hello World'),
                                'Hello World');
          done();
        });
    });
    it('Missing translations', function(done) {
      this.timeout(5000);
      let p = i18next
        .createInstance()
        .use(backend)
        .init({
          backend: {
            url: 'https://translate.stage.zanata.org',
            project: 'i18next-node-zanata-backend',
            'project-version': 'master',
            'project-type': 'gettext',
            cachedir: './test'
          },
          nsSeparator: false,
          keySeparator: false,
          lng: 'ja',
          fallbackLng: false
        });
      p
        .on('loaded', () => {
          assert.strictEqual(p.t('Missing Translations'),
                             'Missing Translations');
          done();
        });
    });
  });
});
