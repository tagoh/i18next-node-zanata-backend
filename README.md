# i18next-node-zanata-backend [![NPM version](https://badge.fury.io/js/i18next-node-zanata-backend.png)](http://badge.fury.io/js/i18next-node-zanata-backend)

i18next-node-zanata-backend is an i18next backend layer for Zanata.

## Getting started

```
#!shell

% npm install i18next-node-zanata-backend
```

### Overview

```js
var i18next = require('i18next');
var Backend = require('i18next-node-zanata-backend');

i18next
  .use(Backend)
  .init(options);
```

### Backend Options

```js
{
  // url to the Zanata server
  url: 'https://translate.zanata.org',

  // API key for Zanata
  'api-key': 'API key',

  // project name
  project: 'project name in zanata',

  // project version
  'project-version': 'version',

  // project type
  'project-type': 'file|gettext|podir|properties|utf8properties|xliff|xml',

  // project config file downloaded from Zanata
  'project-config': '/path/to/zanata.xml',

  // Get more debugging messages. the default is false
  verbose: <boolean>,

  // specify the POT directory to read/write. the default is './po'
  potdir: '/path/to/pot',

  // specify the PO directory to read/write. the default is './po'
  podir: '/path/to/po'
}
```

**NOTE** the key based fallback style is only supported. so you have to set `fallbackLng`, `keySeparator`, and `nsSeparator` as i18next options like:
```js
{
  nsSeparator: false,
  keySeparator: false,
  fallbackLng: false
}
```

See http://i18next.com/translate/keyBasedFallback/ for more details.

## License

MIT
