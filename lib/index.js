/*
 * i18next-node-zanata-backend
 * https://bitbucket.org/tagoh/i18next-node-zanata-backend
 *
 * Copyright (c) 2016 Akira TAGOH
 * Licensedunder the MIT license.
 */
'use strict';

const _ = require('underscore');
const ZanataClient = require('zanata-js').ZanataClient;
const i18nextconv = require('i18next-conv');

const getDefaults = function(args) {
  let ret = _.clone(args);
  ret = _.extend({
    url: 'https://translate.zanata.org'
  }, ret);

  return ret;
};

class Backend {

  constructor(services, options) {
    if (!(this instanceof Backend)) {
      throw new TypeError('Unable to call a class as a function');
    }
    this.init(services, options);
    this.type = 'backend';
  }

  init(services, options, i18nextOptions) {
    this.services = services;
    this.options = getDefaults(options);
  }

  read(lang, ns, cb) {
    let p = new ZanataClient.Project(this.options)
        .on('fail', (e) => cb(e, false))
        .on('data', (d) => {
          i18nextconv.gettextToI18next(lang, d.data)
            .then((r) => cb(null, JSON.parse(r)));
        })
        .on('end', (r) => {});
    if (lang !== 'en')
      p.pullTranslation(null, null, ns, lang,
                        {verbose: this.options.verbose,
                         potdir: this.options.cachedir,
                         podir: this.options.cachedir});
    else
      cb(false, false);
  }

  readMulti(lang, ns, cb) {
    console.log('readMulti');
  }

  create(lang, ns, key, val) {
    console.log('create');
  }

}

Backend.type = 'backend';

module.exports = Backend;
